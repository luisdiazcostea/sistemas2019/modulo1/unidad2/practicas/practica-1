﻿USE practica1;


--1
SELECT * FROM emple;

--2
SELECT * FROM depart;

--3
SELECT DISTINCT e.apellido, e.oficio FROM emple e;

--4
SELECT DISTINCT d.loc, d.dept_no FROM depart d;

--5
SELECT d.dept_no, d.dnombre, d.loc FROM depart d;

--6
SELECT COUNT(*) FROM emple;

--7
SELECT * FROM emple ORDER BY apellido ASC;

--8
SELECT * FROM emple ORDER BY apellido DESC;

--9
SELECT COUNT(*) FROM depart d;

--10
SELECT (SELECT COUNT(*) FROM depart d) + (SELECT COUNT(*) from emple e) AS Total;

--11
SELECT * FROM emple e ORDER BY e.dept_no DESC;

--12
SELECT * FROM emple e ORDER BY e.dept_no DESC, e.oficio ASC;

--13
SELECT * FROM emple e ORDER BY e.dept_no DESC, e.apellido ASC;

--14
SELECT e.emp_no FROM emple e WHERE e.salario>2000;

--15
SELECT e.emp_no, e.apellido FROM emple e WHERE e.salario<2000;

--16
SELECT * FROM emple e WHERE e.salario BETWEEN 1500 AND 2500;

--17
SELECT * FROM  emple e WHERE e.oficio="analista";

--18
SELECT * FROM  emple e WHERE e.oficio="analista" AND e.salario>2000;

--19
SELECT e.apellido, e.oficio FROM emple e WHERE e.dept_no="20";

--20
SELECT COUNT(*) FROM emple e WHERE oficio="vendedor";

--21
SELECT * FROM emple e WHERE e.apellido LIKE "m%" OR "n%";

--22
SELECT * FROM emple e WHERE e.oficio="vendedor" ORDER BY e.apellido ASC;

--23
SELECT e.apellido FROM emple e WHERE e.salario=(SELECT MAX(e.salario) FROM emple e);

--24
SELECT * FROM emple e WHERE e.dept_no=10 AND e.oficio="analista" ORDER BY e.apellido ASC, e.oficio ASC;

--25
SELECT MONTH(fecha_alt) FROM emple e;

--26
SELECT YEAR(fecha_alt) FROM emple e;

--27
SELECT DAY(fecha_alt) FROM emple e;

--28
SELECT e.apellido FROM emple e WHERE salario>2000 OR e.dept_no=20;

--29
SELECT e.apellido, d.dnombre FROM emple e JOIN depart d ON e.dept_no = d.dept_no;

--30
SELECT e.apellido, e.oficio, d.dnombre FROM emple e JOIN depart d ON e.dept_no = d.dept_no ORDER BY e.apellido DESC;

--31
SELECT e.dept_no, COUNT(*) AS NUMERO_DE_EMPLEADOS FROM emple e GROUP BY e.dept_no;

-32
SELECT d.dnombre, COUNT(e.dept_no) AS NUMERO_DE_EMPLEADOS FROM emple e JOIN depart d ON e.dept_no = d.dept_no GROUP BY e.dept_no;

--33
SELECT e.apellido FROM emple e ORDER BY e.oficio ASC, e.apellido ASC;

--34
SELECT e.apellido FROM emple e WHERE e.apellido LIKE "A%";

--35
SELECT e.apellido FROM emple e WHERE e.apellido LIKE "A%" OR e.apellido LIKE "M%";

--36
SELECT * FROM emple e WHERE e.apellido NOT LIKE "%Z";

--37
SELECT * FROM emple e WHERE e.apellido LIKE "A%" AND e.oficio LIKE "E%";
